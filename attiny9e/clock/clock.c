#include <avr/io.h>

int main(void)
{
    DDRB |= _BV(PB2);
    // write signature to CCP to change clock speed
    CCP = 0xD8;

    // modify clock speed wtih no preescaler
    CLKPSR = 0x00;
    
    while(1);
}
