import subprocess
from os import listdir

class gerberObject():
	def __init__(self, path_name):
		self.top_silk = "null"
		self.bot_silk = "null"
		self.silk_foreground = "--foreground=#ffffffff"

		self.top_mask = "null"
		self.bot_mask = "null"
		self.mask_foreground = "--foreground=#FFE169ff"

		self.top_copper = "null"
		self.bot_copper = "null"
		self.copper_foreground = "--foreground=#33cc33ff"

		self.drill = "null"
		self.drill_foreground = "--foreground=#000000ff"

		self.board_outline = "null"
		self.board_outline_foreground = "--foreground=#000000ff"

		self.settings = "gerbv --border=0 --export=png --dpi=1080 --background=#008800 "

		var = listdir(path_name)

		for i in var:
			i = i.replace(" ", "\ ")
			i = path_name + i
			#print i

			#look for top layer
			if i.endswith(".gto"):
				self.top_silk = i

			if i.endswith(".gts"):
				self.top_mask = i

			if i.endswith(".gtl"):
				self.top_copper = i

			if i.endswith(".gbo"):
				self.bot_silk = i

			if i.endswith(".gbs"):
				self.bot_mask = i
				
			if i.endswith(".gbl"):
				self.bot_copper = i
				
			if i.endswith(".drl"):
				self.drill = i

			if i.endswith(".gbr"):
				self.board_outline = i
				
				
			
			#for j in [".gto",".gts",".gtl",".gbo",".gbs",".gbl",".drl",".gbr"]:
			#	if i.endswith(j):
			#		print i'''

	def render_top(self):
		top_cu = self.copper_foreground + " " + self.top_copper + " "
		top_silk = self.silk_foreground + " " + self.top_silk + " "
		top_mask = self.mask_foreground + " " + self.top_mask + " "
		top_drill = self.drill_foreground + " " + self.drill + " "
		top_outline = self.board_outline_foreground + " " + self.board_outline + " "

		cmd = self.settings + top_drill + top_silk + top_mask + top_cu + top_outline

		print cmd

		subprocess.call(cmd, shell=True)




gerber = gerberObject("./")
gerber.render_top()
